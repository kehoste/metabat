#!/bin/bash

oops() { echo "Failed to build samtools!" 1>&2 ; exit 1; }
trap oops 0

set -x
set -e

htslibver=1.7
samver=1.7
samtar=samtools-${samver}.tar.bz2
url=https://github.com/samtools/samtools/releases/download/${samver}/${samtar}
dir=samtools-$samver

[ -f $samtar ] || curl -kL -o $samtar $url
[ -d $dir ] || tar -xvjf $samtar

inst=$1
[ -n "$inst" ] || inst=/usr/local
[ -d "$inst" ] || mkdir $inst
if [ ! -w "$inst" ] 
then
  echo "Please specify a writable install path to build and install samtools / htslib" 1>&2
  exit 1
fi

for d in bin include include/bam lib lib/pkgconfig share share/man share/man/man5 share/man/man1
do
  mkdir -p $inst/$d
done

cd $dir

./configure --prefix=$inst --disable-bz2 --disable-lzma 

cd htslib-$htslibver
make -j8 && make install

cd ..

make -j8
make install
cp libbam.a libst.a $inst/lib
#cp *.h $inst/include/bam

trap "" 0

